# NuNet Network Status

The application is live at https://stats.nunet.io. Current status is [v0.0.1](https://gitlab.com/nunet/nunet-network-status/-/releases) community beta.

NuNet Network Status is a publicly accessible dashboard for displaying global network statistics of NuNet platform.

<img src="artifacts/images/dashboard_view_20200107.png"  width="700">

For more, see:

* [Release notes](https://gitlab.com/nunet/nunet-network-status/-/releases#v0.0.1)
* NuNet webpage at https://nunet.io;
* NuNet introductory [video](https://www.youtube.com/watch?v=ju6dQJdTR3g);
* [NuNet Platform Demo](https://demo.nunet.io) application.

